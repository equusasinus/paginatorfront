import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  

  constructor(private httpClient: HttpClient) { }

  getData(pageIndex = 0, pageSize =  10){
    return this.httpClient.get(environment.baseUrl.concat("/api/users/pageOf")
    .concat("?pageIndex=").concat(pageIndex.toString())
    .concat("&pageSize=").concat(pageSize.toString()));
  }
}
