import { User } from './models/user-model';
import { UsersService } from './services/users.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { PagedUsers } from './models/paged-user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit{
  isDisplay: boolean = false;
  displayedColumns: string[] = ['name', 'email' ,'city', 'tel'];
  dataSource = new MatTableDataSource<User>();
  constructor(private usersService: UsersService){

  }

  ngOnInit(): void {
    this.getData();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;



  getData(pageIndex?: number, pageSize?: number){
    this.isDisplay= true;
    let rep: PagedUsers;
    this.usersService.getData(pageIndex,pageSize).subscribe(
      (response: PagedUsers) => {
        this.dataSource = new MatTableDataSource(response.content);
        this.paginator.length = response.totalElements;
        this.isDisplay = false;
      } ,
      (error)=> {
        console.error(error)
        this.isDisplay = false;
      }

    );
  }

  loadData(event){
    this.getData(this.paginator.pageIndex,this.paginator.pageSize);
  }
}
