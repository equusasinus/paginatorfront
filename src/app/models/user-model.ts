export interface User {
    sequenceId: number;
    name: string;
    email: string;
    tel: string;
    city: string;
  }